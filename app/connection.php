<?php

    //Conexión con DB
    function OpenCon() {
        //Connecting to Database
        $dbhost = "localhost";
        $dbuser = "root";
        $dbpass = "";
        $db = "webform-davidmarquez";
        $conn = new mysqli($dbhost, $dbuser, $dbpass,$db) or die("Connect failed: %s\n". $conn -> error);
        return $conn;
    }
    
    //Cierra conexión
    function CloseCon($conn) {
        $conn -> close();
    }

?>