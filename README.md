<h4>Prueba técnica de conocimientos PHP / MySQL / Javascript</h4>
<br /><br />
DAVID FELIPE MÁRQUEZ GONZÁLEZ
<br /><br />
Desarrollado con PHP 8.2
<br /><br />
Este requerimiento fue desarrollado en ambiente Windows con XAMPP (segùn lo conversado en la videollamada), por lo tanto, se debe revisar la inclusión del archivo de conexión en <strong>app/functions/save_user.php</strong><br /><br />
Adicionalmente, se adjunta la base de datos creada para el desarrollo del requerimiento.<br /><br />
<h5>Tecnologías Utilizadas</h5>
* PHP 8.2 Nativo (básico)<br/>
* Html (Formulario)<br />
* Bootstrap <br />
* Javascript (JQuery, AJAX)<br />