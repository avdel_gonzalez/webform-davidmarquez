//Funcionalidad
//Define formulario e intercepta el submit con prevent Default para validar campos
let user_form = $('#user-info');
$(user_form).on("submit", function(e) {
    e.preventDefault();
    //Limpia errores
    $('#errors').empty();
    if(validateInputs()){
        sendForm();
    }
});

function validateInputs() {
    //Validación de campos. Que todos contengan información
    let full_name = $('#full-name-field').val();
    let username = $('#username-field').val();
    let password = $('#password-field').val();
    let confirm_password = $('#confirm-password-field').val();
    let email = $('#email-field').val();
    let confirm_email = $('#confirm-email-field').val();
    let facebook_username = $('#facebook-username-field').val();
    let twitter_username = $('#twitter-username-field').val();
    let errors = false;

    if(full_name == '') {
        let error_template = '<span class="badge text-bg-danger">Debe llenar el campo Full Name</span>';
        $('#errors').append(error_template);
        errors = true;
    }
    if(username == '') {
        let error_template = '<span class="badge text-bg-danger">Debe llenar el campo Username</span>';
        $('#errors').append(error_template);
        errors = true;
    }
    if(password == '') {
        let error_template = '<span class="badge text-bg-danger">Debe llenar el campo Password</span>';
        $('#errors').append(error_template);
        errors = true;
    }
    if(email == '') {
        let error_template = '<span class="badge text-bg-danger">Debe llenar el campo Email</span>';
        $('#errors').append(error_template);
        errors = true;
    }
    if(confirm_password == '') {
        let error_template = '<span class="badge text-bg-danger">Debe llenar el campo Confirm Password</span>';
        $('#errors').append(error_template);
        errors = true;
    }
    if(confirm_email == '') {
        let error_template = '<span class="badge text-bg-danger">Debe llenar el campo Confirm Email</span>';
        $('#errors').append(error_template);
        errors = true;
    }
    if(facebook_username == '') {
        let error_template = '<span class="badge text-bg-danger">Debe llenar el campo Facebook username</span>';
        $('#errors').append(error_template);
        errors = true;
    }
    if(twitter_username == '') {
        let error_template = '<span class="badge text-bg-danger">Debe llenar el campo Twitter username</span>';
        $('#errors').append(error_template);
        errors = true;
    }
    //Valida que email y password coincidan con sus respectivos campos de confirmación
    if(confirm_password != password) {
        let error_template = '<span class="badge text-bg-danger">Las contraseñas no coinciden</span>';
        $('#errors').append(error_template);
        errors = true;
    }
    if(confirm_email != email) {
        let error_template = '<span class="badge text-bg-danger">Los email no coinciden</span>';
        $('#errors').append(error_template);
        errors = true;
    }
    if(errors){
        return false;
    }
    return true;
}

function sendForm() {
    let data = $('#user-info').serialize();
    //Envío de petición POST
    $.ajax({
        url: 'app/functions/save_user.php',
        method: 'POST',
        data: data,
        dataType: 'json',
        success: function(response) {
            Swal.fire({
                title: 'Gracias!',
                text: 'Usuario "' + $('#full-name-field').val() + '" guardado exitosamente',
                icon: 'success',
                confirmButtonText: 'Aceptar'
            });
        }
    });
}